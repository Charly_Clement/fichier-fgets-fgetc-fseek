<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
    <style>
    body {
        width           : 100%;
        background-color: rgb(225, 213, 228);
        text-align      : center;
        font-size       : 25px;
    }

    .container {
        width          : 90%;
        display        : flex;
        justify-content: space-around;
        margin         : 0 auto;
        flex-direction: row;
    }

    .container .bloc {
        padding: 0 20px;
    }

    .bloc {
        background-color: rgb(218, 189, 224);
        box-shadow      : 0 0 7px grey;
        text-align      : center;
        justify-items   : center;
        padding         : 10px;
        border-radius   : 10px;
    }

    .taille {
        width    : 75%;
        margin   : 0 auto;
        font-size: 30px;
    }

    p {
        font-family: Arial, Helvetica, sans-serif;
    }

    @media (max-width:700px){

        .container{
            flex-direction: column;
        }

        .container .bloc {
            padding: 10px;
            margin: 10px 0;
        }

        .br {
            display:none;
        }

        .taille {
            width    : 85%;
            font-size: 25px;
        }
    }
    </style>
</head>
<body>
        
    <?php

    // Créer un script afin de récupérer toutes les infos de l'Ordonnance.txt qu'a rempli le Dr Strauss
    // Récupérer les coordonnées du patient, celle du docteur, son hopital, la date, les médicaments et la personne qui à fait l'ordonnance
    // Affichez comme y faut les informations dans une jolie page html bien stylisée
    // Lorsque que vous pensez avoir fini, venez me voir avec votre pc que je test votre code
        
    ?>
    
    <!-- écrire le code après ce commentaire -->
        
    <?php
        
            $source = fopen('Ordonnance.txt', 'rb');

    for ($i=0; $i < 3; $i++) { 
        $infoInutiles = fgets($source);
    }
        
            $nomPrenom = fgets($source);
            $rue = fgets($source);
            $CPVille = fgets($source);

    $infoInutiles = fgets($source);

            $medecin = fgets($source);
            $hopital = fgets($source);

            $dateBrute = fgets($source);
            $dateBrute = str_replace('Le ', '', $dateBrute);
            $date = str_replace(',', '', $dateBrute);

    $infoInutiles = fgets($source);
        
            $nbrMedocBrut = fgets($source);
            $nbrMedoc = str_replace('Nombres de médicaments : ', '', $nbrMedocBrut);

    for ($i=0; $i < 3; $i++) { 
        $infoInutiles = fgets($source);
    }

            $listeMedocBrute = '';

            for ($i=0; $i < $nbrMedoc; $i++) { 
                $listeMedocBrute .= fgets($source).'<br>';
            }

            $listeMedoc = str_replace('- ', '', $listeMedocBrute);

    $infoInutiles = fgets($source);

            $redacOrdo = fgets($source);

    ?>

    <h1>Ordonnance</h1>
    
    <div class="container">
        <div class="bloc">
            <p>Information patient :</p>
            <p><?php echo $nomPrenom; ?></p>
            <p><?php echo $rue; ?></p>
            <p><?php echo $CPVille; ?></p>
        </div>

        <div class="bloc">
            <p>Informations médecin</p>
            <span class="br"><br></span>
            <p><?php echo $medecin; ?></p>
            <p><?php echo $hopital; ?></p>
        </div>
    </div>

    <div class="container"><p>Date : <?php echo $date; ?></p></div>

    <div class="bloc taille"><p><?php echo $listeMedoc; ?></p></div>

    <div class="container"><p><?php echo $redacOrdo; ?></p></div>

    <!-- écrire le code avant ce commentaire -->
    
</body>
</html>
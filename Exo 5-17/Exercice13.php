<!DOCTYPE html>
<html lang='fr'>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>

<body>

    <?php

    // fgets ou fgetc ???
    // Affichez la date du fichier notes.txt ( Le 29/01/2020 )

    $source = fopen('notes.txt','rb');

    for ($i = 1; $i <= 13; $i++) {
        echo fgetc($source);
    }

    ?>

</body>

</html>
<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
<body>
        
    <?php

    // fgets ou fgetc ???
    // Affichez seulement la date du fichier notes.txt ( 29/01/2020 )

    $source = fopen('notes.txt','rb');
    fgetc($source);
    fgetc($source);

    for ($i = 0; $i <= 10; $i++) {
        echo fgetc($source);
    }


    ?>

</body>
</html>
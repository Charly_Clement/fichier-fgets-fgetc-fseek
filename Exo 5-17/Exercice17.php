<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
    <style>
    body {
        width           : 100%;
        background-color: rgb(225, 213, 228);
        text-align      : center;
        font-size       : 25px;
    }

    .container {
        width          : 90%;
        display        : flex;
        justify-content: space-around;
        margin         : 0 auto;
        flex-direction: row;
    }

    .container .bloc {
        padding: 0 20px;
    }

    .bloc {
        background-color: rgb(218, 189, 224);
        box-shadow      : 0 0 7px grey;
        text-align      : center;
        justify-items   : center;
        padding         : 10px;
        border-radius   : 10px;
    }

    .taille {
        width    : 75%;
        margin   : 0 auto;
        font-size: 30px;
    }

    p {
        font-family: Arial, Helvetica, sans-serif;

    }

    @media (max-width:700px){

        .container{
            flex-direction: column;
        }

        .container .bloc {
            padding: 10px;
            margin: 10px 0;
        }

        .br {
            display:none;
        }

        .taille {
            width    : 85%;
            font-size: 25px;
        }
    }
    </style>
</head>
<body>
        
    <?php

    // Créer un script afin de récupérer toutes les infos de l'Ordonnance.txt qu'a rempli le Dr Strauss
    // Récupérer les coordonnées du patient, celle du docteur, son hopital, la date,
    // les médicaments et la personne qui à fait l'ordonnance
    // Affichez comme il faut les informations dans une jolie page html bien stylisée
    // Lorsque que vous pensez avoir fini, venez me voir avec votre pc que je test votre code

        $source = fopen('Ordonnance.txt','rb');
        echo fgets($source).'<br><br>';// ligne 1

    echo '<div class="container">';

    // bloc gauche
        echo '<div class="bloc">'.'Informations';
            for ($i = 1; $i <= 4; $i++) {
                echo fgets($source).'<br><br>';// ligne 2,3,4,5
            }
        echo '</div>';
        
        // bloc droite
        echo '<div class="bloc">'.'Informations';
            for ($i = 1; $i <= 3; $i++) {
                
                echo fgets($source).'<br><br>'; //ligne 6,7,8
            }
        echo '</div>';

    echo '</div>'.'<br>';

        echo fgets($source).'<br><br>';// ligne 9, DATE

        $nbrMedocBrut = fgets($source);
        $nbrMedoc = str_replace('Nombres de médicaments : ', '', $nbrMedocBrut);// ligne 11, Le patient ci-dessus à besoin de :

        $listeMedocBrute = '';

    // bloc centre
    echo '<div class="bloc taille"><br>';
        for ($i=0; $i < $nbrMedoc; $i++) { 
            $listeMedocBrute .= fgets($source).'<br>';
        }
    echo '<br></div><br>';

        echo fgets($source);// ligne 15, Ordonnance faite par : Dr Strauss

    ?>

</body>
</html>